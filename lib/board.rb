class Board
  @grid
  @length

  attr_accessor :grid, :length

  def initialize(grid = Board.default_grid)
    @grid = grid
    @length = grid[0].length
  end

  def self.default_grid
    grid = []
    10.times { grid.push(Array.new(10)) }
    grid
  end

  def count
    count = 0

    @grid.each do |row|
      row.each do |position|
        count += 1 if position == :s
      end
    end

    count
  end

  def [](position)
    row, column = position
    @grid[row][column]
  end

  def []=(position, value)
    row, column = position
    @grid[row][column] = value
  end

  def empty?(position = nil)
    if position
      self[position].nil?
    else
      count == 0 ? true : false
    end
  end

  def full?
    count == @length**2 ? true : false
  end

  def place_random_ship
    # raises error is board is full
    raise if full?

    # ensures ship is placed
    random_position = [Random.rand(@length), Random.rand(@length)]
    loop do
      if empty?(random_position)
        break
      else
        random_position = [Random.rand(@length), Random.rand(@length)]
      end
    end

    self[random_position] = :s
  end

  def won?
    empty?
  end
end
