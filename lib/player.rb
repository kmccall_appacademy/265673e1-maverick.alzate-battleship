class HumanPlayer
  @name

  def initialize(name)
    @name = name
  end

  def get_play
    # keep getting input until viable grid position selected
    loop do
      input = gets.chomp
      position = input.scan(/\d/)
      if position == :x
        next
      else
        break
      end
    end
    # return position
    position
  end
end
